# TP1  
 
## Discrétisation d'une sphere + construction des triangles
```c++
void setUnitSphere( Mesh & o_mesh, int nX, int nY )
{

    float x, y, z;
    float row_amount = (2 * M_PI) / nX;
    float col_amount = M_PI / nY; 

    for(int i = (-nX / 2); i <= (nX / 2); i ++) {
        for(int j = 0; j <= nY; j ++) {

            //coordonées d'un vertex
            // j * row_amount = theta, i * col_amount = phi

            x = cos(j * row_amount) * cos(i * col_amount);
            y = sin(j * row_amount) * cos(i * col_amount);
            z = sin(i * col_amount);

            Vec3* vertex = new Vec3(x, y, z);
            //Sphere unitaire -> normales des sommets = les points eux memes
            Vec3* normal = new Vec3(x, y, z);

            //ajout du vertex dans le tableau de vertices
            o_mesh.vertices.push_back(*vertex);
            o_mesh.normals.push_back(*normal);
        }
    }

    //construction des triangles
    // total: nX * nY + nY sommets 
    for(int i = 0; i < (nX * nY) + nX; i ++) {
        //nY + 1 sommets par parallèles donc le sommet directement en dessous est a l'index i + (nX + 1)
        Triangle t1(i, i + 1, i + nX + 1);
        Triangle t2(i + 1, i + nX + 2, i + nX + 1);
        o_mesh.triangles.push_back(t1);
        o_mesh.triangles.push_back(t2);
    }

}

 ```

# TP3
3 types de courbes paramétriques:

1. **Courbe cubique d'hermite** (pt de départ + v0, pt d'arrivée + v1)
    discrétisation: 
    ```c++ 
        std::vector<Vec3> HermiteCubicCurve(Vec3 P0, Vec3 P1, Vec3 V0, Vec3 V1, long nbU) {
            std::vector<Vec3> points;
            Vec3 Pu(0., 0., 0.);
            float u = 0.;

            for(int i = 0; i < nbU; i ++) {
                u = (float)i/nbU;      // pas

                Pu = (pow(2*u, 3) - (pow(3*u, 2)) + 1) * P0 +
                    (-pow(2*u, 3) + (pow(3*u, 2))) * P1 +
                    ((pow(u, 3) - 2*pow(u, 2)) + u) * V0 + 
                    (pow(u, 3) - pow(u, 2)) * V1;

                points.push_back(Pu);
            }

            return points;
        }

    ```
2. **Courbe de Bézier par les polynômes de Bernstein**

    un point de la courbe est une combinaison affine des points de contrôles, par définition on a:

    $p(u) =$ $\sum_{i=0}^n B_i^n(u)P_i$ avec 
    1. $P_i$ les n+1 points de controle
    2. $B_i^n$ les polynômes de bernstein de degré n.
    3. $B_i^n(u) =$ $\frac{n!}{i!(n-i)!}$ $u^i(1-u)^{n-i}$, exemple pour n=3:

        $B_0^3(u) = (1-u)^3$

        $B_1^3(u) = 3u(1-u)^2$

        $B_2^3(u) = 3u^2(1-u)$

        $B_3^3(u) = u^3$

    discrétisation:
    ```c++
        std::vector<Vec3> BezierCurveByBernstein(std::vector<Vec3> TabControlPoint, long nbControlPoint, long nbU) {

        std::vector<Vec3> points;
        float u = 0.;   
        int n = nbControlPoint - 1;
        float Bern = 0.;

        for(int k = 0; k < nbU; k ++) {
            Vec3 Pu(0., 0., 0.);
            u = (float) k/(nbU - 1); //pas
            for(int i = 0; i < nbControlPoint; i ++) {
                /*  bern est le monome de bernstein en i */
                Bern = fact(n)/(fact(i) * fact(n - i)) * pow(u, i) * (pow( 1 - u, n - i));
                Pu += Bern * TabControlPoint[i];   
            }

            points.push_back(Pu);
        }

        return points;       
    }


    ```

3. **Courbe de bézier par l'algorithme de Casteljau**
![aze](screen_casteljau.png)

discrétisation: 
```c++
Vec3 getPointCasteljau(float u, int k, int i, std::vector<Vec3> P) {
    if(k==0)
        return P[i];

    return (1-u) * getPointCasteljau(u, k-1, i, P) + u * getPointCasteljau(u, k-1, i+1, P);
    
}


std::vector<Vec3> BezierCurveByCasteljau(std::vector<Vec3> ctrl, std::vector<std::vector<Vec3>> &constrP, long nbCp, long nbU) {
    std::vector<Vec3> points;
    float u;
    int n = nbCp - 1;
    for(int i = 0; i < nbU; i ++) {
        u = (float) i / (nbU-1);

        //ajout du point calculé par récursion
        points.push_back(getPointCasteljau(u, n, 0, ctrl));

        //construction des segments ayant servis pour le calcul du point u 
        std::vector<Vec3> segment;
        int nbVertex = 0;

        for(int i = 1; i < n; i ++) {
            for(int k = 0; k <= n-i; k++) {
                segment.push_back(getPointCasteljau(u, i, k, ctrl));
                nbVertex ++;

                //avoir des segments continus sur une même itération
                if(nbVertex == 2){
                    Vec3 lastPoint = segment[1];
                    constrP.push_back(segment);
                    segment.clear();

                    segment.push_back(lastPoint);

                    nbVertex = 1;
                }

            }
            
            nbVertex = 0;
            segment.clear();
        }
    }

    return points;
}

 ```

# TP4

1. **surface réglée** 
    
    2 courbes de bézier en entrée -> un ensemble de courbes iso-paramétriques.

    discrétisation:
    ```c++
    std::vector<std::vector<Vec3>> getRuledSurface(std::vector<Vec3> bezierctrl1, std::vector<Vec3> bezierctrl2, float u, float v) {
        std::vector<Vec3> P = BezierCurveByBernstein(bezierctrl1, bezierctrl1.size(), u);
        std::vector<Vec3> Q = BezierCurveByBernstein(bezierctrl2, bezierctrl2.size(), u);
        
        std::vector<std::vector<Vec3>> ruledSurface;

        for(int i = 0; i <= v; i ++) {
            std::vector<Vec3> line;
            for(int j = 0; j < u; j ++) {
                float v_amount = (float) i/v;       // v_amount doit ête compris entre 0 et 1

                Vec3 pos = (1-v_amount)*P[j] + v_amount*Q[j];
                line.push_back(pos);
            }

            ruledSurface.push_back(line);
        }

        return ruledSurface;
    }

    ```
2. **surface de Bézier par les polynômes de bernstein** (carreaux surfaciques)
```c++ 
    std::vector<std::vector<Vec3>> bezierSurfaceByBernstein(std::vector<std::vector<Vec3>> gridCP, int nbCPu, int nbCPv, int nbU, int nbV) {
    std::vector<std::vector<Vec3>> bezierSurface;
    float degV = nbCPv-1;
    float degU = nbCPu-1;
    bezierSurface.clear();

    for(int i = 0; i < nbV; i ++) {
        std::vector<Vec3> surfLine;
        float v = (float) i / (nbV-1);

        for(int j = 0; j < nbU; j ++) {
            Vec3 acc(0., 0., 0.);
            // calculer les Bernstein avec le nombre de points de controles en u et v

            float u = (float) j / (nbU-1);
            for(int n = 0; n < nbCPv; n ++) {


                float Bi = getBern(n, degV, u);
                for(int m = 0; m < nbCPu; m ++) {

                    float Bj = getBern(m, degU, v);
                    acc += Bi * Bj * gridCP[n][m];
                }

            }

            surfLine.push_back(acc);
            acc = Vec3(0., 0., 0.);
        }

        bezierSurface.push_back(surfLine);

        surfLine.clear();
    }
    return bezierSurface;
}

```



