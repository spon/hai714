## TP5_simplification

La fonction simplify à été implantée avec succès après quelques, une lecture plus approfondie de la documentation de la classe **std::vector** m'aurait épargné des heures de débogage: à la ré-indexation des triangles s'il fallait éliminer un triangle j'utilisais la méthode `` erase(iterator pos) `` sur le triangle courant or cette méthode ré-indexe tous les autre triangles contenus dans la liste en détruisant le triangle à l'indice *pos*. 

Un grand nombre de triangles n'étaient alors pas traités ce qui avait pour effet de laisser des *trous* dans mon maillage simplifié: 

| ![maillage_simplifié_raté](/home/spon/Images/elephant_trous.png) |
| :----------------------------------------------------------: |
| *maillage simplifié (16x16x16) en utilisant vector.erase()*  |

L'utilisateur peut modifier la résolution de la grille en ajoutant en paramètre de **simplify()** la résolution de la grille des représentants (```mesh.simplify(16);``` (ligne 901)) de base la résolution est 16. 

| ![maillage_simplifié](/home/spon/Images/elephant_propre.png) |
| :----------------------------------------------------------: |
|    *maillage simplifié (16x16x16) correctement construit*    |

