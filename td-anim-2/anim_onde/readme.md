## Animation (2)

Pour l'équation de l'onde j'ai utilisé le **cosinus** en fonction du temps qui produit une courbe **sinusoïdale** dont on peut moduler l'amplitude en la multipliant par un scalaire **A**. Je n'ai pas de vitesse calculée, simplement un **décalage temporel** entre 2 "lignes" de la grille pour produire cet effet de vague.

L'amplitude varie en fonction du temps: ``` let A = Math.min(10/t, 1); ``` de base l'amplitude diminue au bout de 10 secondes, l'utilisateur peut modifier cette durée.

Comme la "vague" se déplace il fallait alors faire en sorte que la caméra la suive en appliquant une **translation** sur la matrice de **projection**, cette translation prend en paramètre le **temps écoulé depuis le début de l'animation**.

Le slider **Zoom** permet de modifier le nombre de points dans la grille, le manipuler donne un effet de **zoom** car les transformations appliquées à la caméra sont calculées en fonction de la résolution de la grille.

