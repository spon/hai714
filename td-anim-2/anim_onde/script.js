function randColor() {
    return [Math.random(), Math.random(), Math.random(), 1];
}




function radToDeg(r) {
    return r * 180 / Math.PI;
}

function degToRad(d) {
    return d * Math.PI / 180;
}

function grid(gl, size, res, color_used, t) {
    let vertices_grid = new Array(res*res);
    let vertices_line = new Array(res);
    vertices_line.fill(0);
    vertices_grid.fill(0);

    console.log(`input t = ${t}`);
    let T = 0.5;
    let A = Math.min(10/t, 1);            // amplitude de l'onde
    const position_buffer = gl.createBuffer();

    for(let j = 0; j < res; j ++) {

        //construction d'une ligne de l'onde
        for(let i = 0; i < res; i ++) {
            vertices_line[i] = [i*size , 1 + A * Math.cos(t), -(t*size)];
      
        }
        t += T;                             // ajout d'un décalage entre 2 lignes
        vertices_grid[j] = (vertices_line);
        vertices_line = new Array(res);
    }

    let vertices_array = vertices_grid.flat();
    gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices_array.flat()), gl.STATIC_DRAW);

    return {
        toLookAt : vertices_grid[Math.floor(res/2)][Math.floor(res/2)],
        position : position_buffer,
        color : color_used,
        nb_vertices : res*res,
        dimension : 3,
        res: res,
        xoffset: res*size/2
    }
}


main();

function main() {
    const canvas = document.querySelector("#glCanvas");
    const bold_txt = document.querySelector("#ttl");

    let M1 = mat4.create();
    mat4.identity(M1);
    bold_txt.textContent = `TP6: animation onde`;

    const gl = canvas.getContext("webgl");
    const res_btn = document.querySelector("#res");

    // const edges = document.querySelector("#edges");


    if(!gl){
        console.log("impossible d'initialiser webGL!!");
        return;
    }

    // Définir la couleur d'effacement comme étant le noir, complètement opaque
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Effacer le tampon de couleur avec la couleur d'effacement spécifiée
    gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);


    //shaders

    const vertex_shaders = dedent
        `
        attribute vec4 vertex_position;

        uniform mat4 model_view_matrix;
        uniform mat4 projection_matrix;


        void main() {
            gl_Position =  projection_matrix * model_view_matrix * vertex_position;
            gl_PointSize = 5.0;
        }
        `;

    const fragment_shaders = dedent
        `
        uniform highp vec4 col;
        void main() {
            gl_FragColor = col;
        }
        `;

    
    
    // Renvoie le shader crée et compilé
    function load_shader(gl, type, source) {
        const shader = gl.createShader(type);

        //code source envoyé à l'objet shader
        gl.shaderSource(shader, source);

        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.log("erreur à la compilation des shaders."+ gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
            return null;
        }

        return shader;
    }



    // Initialise le programme shader en liant les vertex_shader et fragment_shader
    function init_shader_program(gl, v_shader_src, f_shader_src) {
        const vertex_shader = load_shader(gl, gl.VERTEX_SHADER, v_shader_src);
        const fragment_shader = load_shader(gl, gl.FRAGMENT_SHADER, f_shader_src);


        // Création du programme shader avec le vertex_shader et fragment_shader
        const shader_program = gl.createProgram();
        gl.attachShader(shader_program, vertex_shader);
        gl.attachShader(shader_program, fragment_shader);
        gl.linkProgram(shader_program);

        if (!gl.getProgramParameter(shader_program, gl.LINK_STATUS)) {
            console.log("impossible d'initialiser le programme shader!!");
            return null;
        }
        
        return shader_program;
    }

  
   
    function draw_scene(gl, program_info, buffers, rot, z_amount) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);  // effacement en noir, complètement opaque
        gl.clearDepth(1.0);                 // tout effacer
        gl.enable(gl.DEPTH_TEST);           // activer le test de profondeur
        gl.depthFunc(gl.LEQUAL);            // les choses proches cachent les choses lointaines

        // effacer le canvas avant de dessiner dessus
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


        // Créer une matrice de perspective, une matrice spéciale qui est utilisée pour
        // simuler la distorsion de la perspective dans une caméra.
        // Notre champ de vision est de 90 degrés, avec un rapport largeur/hauteur qui
        // correspond à la taille d'affichage du canvas ;
        // et nous voulons seulement voir les objets situés entre 0,1 unité et 100 unités
        // à partir de la caméra.

        const fieldOfView = 75 * Math.PI / 180;   // en radians
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100.0;
        let projection_matrix = mat4.create();

        
        // projection_matrix -> camera
        mat4.perspective(
            projection_matrix,
            fieldOfView,
            aspect,
            zNear,
            zFar
        );

        let model_view_matrix = mat4.create();
        

        mat4.rotateY(projection_matrix, projection_matrix, Math.PI / 3);
        mat4.translate(projection_matrix, projection_matrix, [0, 0., z_amount]);

        //reculer l'objet
        mat4.translate(model_view_matrix, model_view_matrix, [-buffers.xoffset, -buffers.res/8, 2]);

        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
        // extraire 3 (point en 3D) valeurs par itération, pas d'offset, pas de stride, pas de normalisation (false)
        gl.enableVertexAttribArray(program_info.attrib_location.vertex_position);
        gl.vertexAttribPointer(
            program_info.attrib_location.vertex_position,
            buffers.dimension,
            gl.FLOAT,
            false,
            0,
            0
        );


        gl.useProgram(program_info.program);


        gl.uniformMatrix4fv(
            program_info.uniform_location.projection_matrix,
            false,
            projection_matrix
        );

   
        gl.uniformMatrix4fv(
            program_info.uniform_location.model_view_matrix,
            false,
            model_view_matrix
        );
        
        // couleur unique pour le disque (uniform)
        const color = buffers.color;
        gl.uniform4f(
            program_info.uniform_location.v_col,
            false,
            color[0],
            color[1],
            color[2],
            color[3]
        );

        console.log(buffers.nb_vertices);
        gl.drawArrays(gl.POINTS, 0, buffers.nb_vertices);

    }

    const shader_prog = init_shader_program(gl, vertex_shaders, fragment_shaders);
    // Objet utile pour modifier les valeurs des attributs et uniforms 
    // dans les fragments.
    const program_info = {
        program : shader_prog,

        attrib_location : {
            vertex_position : gl.getAttribLocation(shader_prog, 'vertex_position'),
        },

        uniform_location : {
            projection_matrix : gl.getUniformLocation(shader_prog, 'projection_matrix'),
            model_view_matrix : gl.getUniformLocation(shader_prog, 'model_view_matrix'),
            scaling_matrix : gl.getUniformLocation(shader_prog, 'scaling_matrix'),
            v_col: gl.getUniformLocation(shader_prog, 'col')
        }
    };
    
    

    let sz = 1;
    let current_res = 30;
    let col = randColor();
    let res = parseFloat(res_btn.value);

    let then = 0;
    function Animation() {
        requestAnimationFrame(function fct(time) {
            time *= 0.001;
            let deltaTime = time  - then;       // temps passé entre 2 frames
            then = time;
            let rot = Math.PI /  2 *deltaTime;
            let frameRate = 1/deltaTime;
            res_btn.addEventListener("input", (event) => {
                res = parseFloat(event.target.value);
            });
            let g = (grid(gl, sz, res, col, time));

            draw_scene(gl, program_info, g, rot, time);
            console.log(rot)
            
            
            id = requestAnimationFrame(fct);



        });
    }

    
    Animation();

}