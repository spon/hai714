
class Chain{
	constructor(name, points){
		this.name = name;
		this.points = points;
		this.next = new Array();
		this.previous = new Array();
	}


// ajout une autre chaine en l'attachant au dernier élément de cette chaine
// lorsque begin == true le chemin va du dernier élément de cette chaine au premier élément de l'autre chaine.
// autrement il va vers le dernier élément de l'autre chaine
	addAfter(anotherChain, begin){
		let b = begin ?? true;
		this.next.push({c:anotherChain, b:b});
	}

// ajout une autre chaine en l'attachant au premier élément de cette chaine
// lorsque begin == true le chemin va du dernier élément de cette chaine au premier élément de l'autre chaine.
// autrement il va vers le dernier élément de l'autre chaine	
	addBefore(anotherChain, begin){
		let b = begin ?? false;
		this.previous.push({c:anotherChain, b:b});
	}

//liens entre les "sous-chaines"
	drawBridges(elt, displayer, colors){
		let from, to;
		this.next.forEach(elt => {
			from = elt.b ? elt.c.points[0]: elt.c.points[elt.c.points.length - 1];
			to = this.points[this.points.length -1];
			displayer.mDrawLine(from, to, "#F00");
//			console.log("drawBridges "+this.name);
		});
		this.previous.forEach(elt => {
			from = elt.b ? elt.c.points[0]: elt.c.points[elt.c.points.length - 1];
			to = this.points[0];
			displayer.mDrawLine(from, to, "#F00");
//			console.log("drawBridges "+this.name);
		});
	}

	draw(displayer, colors, incrementalDrawing){
		displayer.drawLineStrip(this.points, colors, incrementalDrawing);
		displayer.drawPoints(this.points, colors, true);
		this.drawBridges(this, displayer, colors);
		this.previous.forEach(function(elt, idx, array){
			elt.c.draw(displayer, colors, true);
		});
		this.next.forEach(function(elt, idx, array){
			elt.c.draw(displayer, colors, true);
		});
	}

}

class Squelette{

	constructor(){
	this.unitx = 75; this.unity = 75; this.margX = 10; this.margY = 10;
		this.leg1 = new Chain("left leg", this.initLeg("", 20, true));
		this.leg2 = new Chain("right leg", this.initLeg("", 10, false));
		this.arm1 = new Chain("left arm", this.initArm("", 30, true));
		this.arm2 = new Chain("right arm", this.initArm("", 50, false));
		this.tronc = new Chain("tronc", this.initTrunk("", 0));
		this.head = new Chain("head", this.initHead("", 40));
		this.tronc.addBefore(this.leg1);
		this.tronc.addBefore(this.leg2);
		this.tronc.addAfter(this.arm1, false);
		this.tronc.addAfter(this.arm2, false);
		this.tronc.addAfter(this.head);
	}

	draw(displayer, colors){
		console.log(this.tronc.points);
		this.tronc.draw(displayer, colors, false);
	}

	initHead(label, idx){
		let points = new Array();
		let x = 2, 
			y = [8.5];
		 for (let i = 0; i < y.length; i++){
		 		points.push(new Point(x*this.unitx+this.margX, y[i]*this.unity+this.margY, label+idx));
		 		//console.log("init tronc " + points[i].x + " "+ points[i].y);
		 		idx++;
		 }
		return points;
	}

	initTrunk(label, idx){
		let points = new Array();
		let x = 2, 
			y = 5.7;
		 for (let i = 0; i < 6; i++){
		 		points.push(new Point(x*this.unitx+this.margX, (y+0.4*i)*this.unity+this.margY, label+idx));
		 		//console.log("init tronc " + points[i].x + " "+ points[i].y);
		 		idx++;
		 }
		return points;
	}

	//left=true pour le côté gauche du modèle (côté droit de la fenêtre à l'écran quand le modèle fait face)
	initLeg(label, idx, left){
		let points = new Array();
		let x = left ? 3 : 1, 
		y = [1.5,3.2,5];
		for (let i = 0; i < y.length; i++){
			points.push(new Point(x*this.unitx+this.margX, y[i]*this.unity+this.margY, label+idx));
			//console.log(left + " " + points[i].x + " "+ points[i].y);
			idx++;
		}
		return points;
	}
	//left=true pour le côté gauche du modèle (côté droit de la fenêtre à l'écran quand le modèle fait face)
	initArm(label, idx, left){
		let points = new Array();
		let x = left ? [3.5, 3.3, 3.2] : [.5, .7, .9], 
			y = [4.5,5.8,7.5];
		 for (let i = 0; i < y.length; i++){
		 		points.push(new Point(x[i]*this.unitx+this.margX, y[i]*this.unity+this.margY, label+idx));
		 		//console.log(left + " " + points[i].x + " "+ points[i].y);
		 		idx++;
		 }
		return points;
	}


}

function normalize(pt){
	let norm = Math.sqrt((pt[0] * pt[0]) + (pt[1] * pt[1]));
	
	// console.log(`x,y: ${pt[0]},${pt[1]}`);
	let x = pt[0] / norm;
	let y = pt[1] / norm;
	
	// console.log(`norme ${norm}`);
	console.log(`normalized x,y: ${x},${y}`);
	pt[0] = x;
	pt[1] = y;
	return [x,y];
}

class AnimatedSkeleton {
	constructor(skeleton) {
		this.points = new Array();
		this.basis = {};
		this.points.push(skeleton.tronc);
		this.points.push(skeleton.leg2);
		this.points.push(skeleton.leg1);
		this.points.push(skeleton.arm1);
		this.points.push(skeleton.arm2);
		this.points.push(skeleton.head);

		this.points.forEach((part) => {
			console.log(part.points);
			part.points.forEach((joint, index) => {
				if(index == 0){

					this.basis[joint.label] = 
					{
						i: new Point(0, 1),
						j: new Point(1, 0)
					}

				}else{
					
					
					let x = joint.x - part.points[index - 1].x;
					let y = joint.y - part.points[index - 1].y;
					
					let f_edge = [x,y];
					// console.log(`x,y found: [${x}, ${y}]`);
					// console.log(`${joint.x} - ${part[index-1].x}`);
					let edge = normalize(f_edge);
					let previousBasis = this.basis[part.points[index-1].label];

					console.log(`angle entre ${edge[0]}, ${edge[1]}; et ${previousBasis.i.x},${previousBasis.i.y}`);
					let cosAlpha = (previousBasis.i.x * edge[0] + previousBasis.i.y * edge[1]);	// cos(angle) entre le vect et i de l'ancienne base
					
					let alpha = Math.acos(cosAlpha);

					console.log(`cos(alpha) = ${cosAlpha}, alpha=${alpha}`);
					
					x = previousBasis.i.x;
					y = previousBasis.i.y

					let basisX = x*cosAlpha - y*Math.sin(alpha);
					let basisY = y*cosAlpha + x*Math.sin(alpha);

					// faire une matrice de rotation 2D avec l'angle alpha trouvé
					//appliquer cette transformation à la previous base pour obtenir la base courante.
					
					let i = new Point(basisX, basisY);
					let j = new Point(-i.y, i.x);
					this.basis[joint.label] =
					{
						i: i,
						j: j	
					};
		
				}

			});
		});

	}

	draw(displayer, colors){
		console.log(this.points[0]);
		this.points[0].draw(displayer, colors, false);
	}

	rotateJoint(jointLabel, rot) {

	}

}