const algoEnvConv = {demiPlan: 0, jarvis:1, graham:2};

class EnveloppeConvexe{
	constructor(lesPoints, a){
		this.algo = a ?? algoEnvConv.jarvis;
		this.points = lesPoints;
		this.setAlgo(this.algo);
	}

	getElements (){
		let ec = new Array();
		for (let i=0; i < this.envconv.length; i++){
			ec.push(this.points[this.envconv[i]]);
		}
		return ec;
	}

	setAlgo(idAlgo){
		this.algo = idAlgo;
		switch (this.algo){
			case algoEnvConv.demiPlan:
				this.envconv = this.algoDemiPlan(this.points);
				console.log(this.envconv);
				break;
			case algoEnvConv.jarvis:
				this.envconv = this.algoJarvis(this.points);
				break;
			case algoEnvConv.graham:
				this.envconv = this.algoGraham(this.points);
				break;	
			default:
				console.log("algo non défini => algo jarvis utilisé")
				this.envconv = this.algoDemiPlan(this.points);
				break;
		}
	}

	findMinIdx(points){
		let imin = 0;
		for(let i = 1; i < points.length; i++){
			if (points[i].y < points[imin].y)
				imin = i;
		}
		return imin;
	}

	determinant(v1,v2){
		return v1.x * v2.y - v1.y * v2.x;
	}

	detSign(v1,v2){
		let d = this.determinant(v1,v2);
		if (d > 0) return 1;
		if (d == 0) return 0;
		return -1;
	}

	vect(a,b){
		return{x:b.x-a.x,y:b.y - a.y}
	}

	// return 0 si les points sont alignés, -1 pour un tour horaire (droit=horaire), +1 pour un tour gauche (=direct=trigo=antihoraire)
	tour(a, b, c){
		let u = this.vect(a, b);
		let v = this.vect(a, c);
		return this.detSign(u, v);

	}

	//return le premier point différent de points[x] et points[y].
	findFirst(points, x, y){
		let n = points.length;
		for(let i = 0; i < n; i ++){
			if(points[i] != points[x] && points[i] != points[y])
				return points[i];
		}
	}

	findNext(points, p) {
		let q = this.findFirst(points, p, p);
		let resultidX = points.indexOf(q);
		let n = points.length;
		for(let i = 0; i < n; i ++){
			if(points[i] != q && points[i] != points[p]){
				
				if(this.tour(points[p], points[i], q) > 0){
					q = points[i];
					resultidX = i;
				}
			}
		}	
		
		return resultidX;

	}

	algoDemiPlan(points){
		console.log("algo d'intersection des demi-plans")
		let envconv = new Array();
		let n = points.length;
		let current, mc, k, first;
		let previous;

		for(let i = 0; i < n; i ++) {
			for(let j = i + 1;  j < n; j ++) {
				mc = true;
				k = 0;
				
				do{
					if(k == 0) {
						first = this.findFirst(points, i, j);

						console.log(`first=${first.x}`);
						previous = this.tour(points[i], points[j], first);
					}

					if(k != i && k != j) {
						current = this.tour(points[i], points[j], points[k]);

						if(current == 0) {
							console.error("pts alignés");
						}else if(current != previous) {
							mc = false;
						}
					}

					k ++;

				}while((k < n) && mc);
				
				if(k == n && previous == current) {
					if(current > 0)
						envconv.push(i, j);
					if(current < 0) 
						envconv.push(j, i);
					if(current == 0) 
						console.error("points alignés");

				}
			}
		}

		return envconv;
	}

	algoJarvis(points){
		 //todo
		console.log("algo Jarvis")
		let min = this.findMinIdx(points);
		let envconv = [min];
		let courant;
		let previous = min;

		do {
			if(envconv.length % 2 == 0)			// jonctions entre les segments
				envconv.push(previous);

			courant = this.findNext(points, previous);
			envconv.push(courant);
			previous = courant;

		}while(courant != min);


		return envconv;
	}

	algoGraham(points){
		let imin = this.findMinIdx(points);
		let ec = new Array();
		ec[0] = imin;

		//todo

		return ec;
	}
}