# TP surfaces paramétriques

Les 3 exercices ont été traités.

1. '**+**' et '**-**' permettent de modifier la position d'un point des différents polygones de controles (tracés en blanc).
2. '**c**' permet d'afficher / masquer une surface cylindrique (tracée en vert).
3. '**u**' et '**v**' permettent d'ajouter des points en ligne/colonne.
4. '**b**' affiche/masque la surface de bézier (tracée en bleu). 
5. '**r**' affiche/masque la surface réglée dont les courbes de bézier sont tracées en rouge et bleu (tracée en vert).
6. '**p**' ajoute/supprime un point de controle.
