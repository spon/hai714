// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include <math.h>
#include "src/Vec3.h"
#include "src/Camera.h"

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;


// ------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}




// ------------------------------------
// rendering.
// ------------------------------------


void DrawCurve(std::vector<Vec3> TabPointsOfCurve, long nbPoints, Vec3 line_color) {
    glBegin(GL_LINE_STRIP);
    glColor3f(line_color[0], line_color[1], line_color[2]);
    float x, y, z;

    for(int i = 0; i < nbPoints; i ++) {
        x = TabPointsOfCurve[i][0];
        y = TabPointsOfCurve[i][1];
        z = TabPointsOfCurve[i][2];
        glVertex3f(x, y ,z);
    }

    glEnd();
}

void DrawLines(std::vector<std::vector<Vec3>> TabPointsOfCurve, long nbPoints, Vec3 line_color) {
    glBegin(GL_LINES);
    Vec3 color = line_color;
    glColor3f(color[0], color[1], color[2]);

    for(size_t i = 0; i < TabPointsOfCurve.size(); i ++) {
        Vec3 v1 = TabPointsOfCurve[i][0];
        Vec3 v2 = TabPointsOfCurve[i][1];
        glVertex3f(v1[0], v1[1] ,v1[2]);
        glVertex3f(v2[0], v2[1] ,v2[2]);
    }

    glEnd();
}

void DrawSurface(std::vector<std::vector<Vec3>> pointS, Vec3 surf_color) {
    int v = pointS.size();      // points par colonnes
    int u = pointS[0].size();   // points par lignes

    std::vector<std::vector<Vec3>> vert;

    for(int i = 0; i < u; i ++) {
        std::vector<Vec3> col;
        for(int j = 0; j < v; j ++){
            col.push_back(pointS[j][i]);
        }

        vert.push_back(col);
    }

    for(int i = 0 ; i < v; i ++){
        DrawCurve(pointS[i], u, surf_color);
    }

    for(int i = 0 ; i < u; i ++) {
        DrawCurve(vert[i], v, surf_color);
    }

}

void drawCylindricalCurve(std::vector<std::vector<Vec3>> curve, float v) {
    glBegin(GL_LINES);
    glColor3f(0., 1., 0.);

    for(size_t i = 0; i < curve.size(); i ++) {
        for(size_t j = 0; j < curve[i].size(); j ++) {
            glVertex3f(curve[i][j][0], curve[i][j][1], curve[i][j][2]);
        }
    }

    glEnd();


}

std::vector<Vec3> HermiteCubicCurve(Vec3 P0, Vec3 P1, Vec3 V0, Vec3 V1, long nbU) {
    std::vector<Vec3> points;
    Vec3 Pu(0., 0., 0.);
    float u = 0.;

    for(int i = 0; i < nbU; i ++) {
        u = (float)i/nbU;

        Pu = (pow(2*u, 3) - (pow(3*u, 2)) + 1) * P0 +
            (-pow(2*u, 3) + (pow(3*u, 2))) * P1 +
            ((pow(u, 3) - 2*pow(u, 2)) + u) * V0 + 
            (pow(u, 3) - pow(u, 2)) * V1;
        points.push_back(Pu);
    }
    return points;
}

int fact(int a){
    if(a == 0)
        return 1;
    return a * fact(a - 1);
}

float getBern(int i, int nbCP, float p) {
    return fact(nbCP)/(fact(i) * fact(nbCP - i)) * pow(p, i) * (pow( 1 - p, nbCP - i));
}

std::vector<Vec3> BezierCurveByBernstein(std::vector<Vec3> TabControlPoint, long nbControlPoint, long nbU) {
    std::vector<Vec3> points;
    float u = 0.;   
    int n = nbControlPoint - 1;
    float Bern = 0.;

    for(int k = 0; k < nbU; k ++) {
        Vec3 Pu(0., 0., 0.);
        u = (float) k/(nbU - 1);
        for(int i = 0; i < nbControlPoint; i ++) {
            // Bern = fact(n)/(fact(i) * fact(n - i)) * pow(u, i) * (pow( 1 - u, n - i));
            Bern = getBern(i, n, u);
            Pu += Bern * TabControlPoint[i];   
        }

        points.push_back(Pu);
    }

    return points;       
}

Vec3 getPointCasteljau(float u, int k, int i, std::vector<Vec3> P) {
    if(k==0)
        return P[i];

    return (1-u) * getPointCasteljau(u, k-1, i, P) + u * getPointCasteljau(u, k-1, i+1, P);
    
}

std::vector<Vec3> BezierCurveByCasteljau(std::vector<Vec3> ctrl, std::vector<std::vector<Vec3>> &constrP, long nbCp, long nbU) {
    std::vector<Vec3> points;
    float u;
    int n = nbCp - 1;
    for(int i = 0; i < nbU; i ++) {
        u = (float) i / (nbU-1);

        //ajout du point calculé par récursion
        points.push_back(getPointCasteljau(u, n, 0, ctrl));

        //construction des segments ayant servis pour le calcul du point u 
        std::vector<Vec3> segment;
        int nbVertex = 0;

        for(int i = 1; i < n; i ++) {
            for(int k = 0; k <= n-i; k++) {
                segment.push_back(getPointCasteljau(u, i, k, ctrl));
                nbVertex ++;

                //avoir des segments continus sur une même itération
                if(nbVertex == 2){
                    Vec3 lastPoint = segment[1];
                    constrP.push_back(segment);
                    segment.clear();

                    segment.push_back(lastPoint);

                    nbVertex = 1;
                }

            }
            
            nbVertex = 0;
            segment.clear();
        }
    }

    return points;
}

std::vector<std::vector<Vec3>> getCylindricalSurface(std::vector<Vec3> TabControlPoint, Vec3 V0,float u, float v) {
    int ctrlPtsSize = TabControlPoint.size();
    std::vector<Vec3> curve = BezierCurveByBernstein(TabControlPoint, ctrlPtsSize,  u);
    std::vector<std::vector<Vec3>> isoCurves;
    isoCurves.push_back(curve);

    float vert_amount;

    for(int i = 1; i < v; i ++) {
        vert_amount = (float) i / v;
        std::vector<Vec3> curveAti;
        curveAti.resize(u);

        for(int k = 0; k < u; k ++) {
            curveAti[k] = isoCurves[i-1][k] + vert_amount*V0;
        }

        isoCurves.push_back(curveAti);
        
    }

    return isoCurves;
}

std::vector<std::vector<Vec3>> getRuledSurface(std::vector<Vec3> bezierctrl1, std::vector<Vec3> bezierctrl2, float u, float v) {
    std::vector<Vec3> P = BezierCurveByBernstein(bezierctrl1, bezierctrl1.size(), u);
    std::vector<Vec3> Q = BezierCurveByBernstein(bezierctrl2, bezierctrl2.size(), u);
    
    std::vector<std::vector<Vec3>> ruledSurface;

    for(int i = 0; i <= v; i ++) {
        std::vector<Vec3> line;
        for(int j = 0; j < u; j ++) {
            float v_amount = (float) i/v;       // v_amount doit ête compris entre 0 et 1

            Vec3 pos = (1-v_amount)*P[j] + v_amount*Q[j];
            line.push_back(pos);
        }

        ruledSurface.push_back(line);
    }

    return ruledSurface;
}

std::vector<std::vector<Vec3>> bezierSurfaceByBernstein(std::vector<std::vector<Vec3>> gridCP, int nbCPu, int nbCPv, int nbU, int nbV) {
    std::vector<std::vector<Vec3>> bezierSurface;
    float degV = nbCPv-1;
    float degU = nbCPu-1;
    bezierSurface.clear();

    for(int i = 0; i < nbV; i ++) {
        std::vector<Vec3> surfLine;
        float v = (float) i / (nbV-1);

        for(int j = 0; j < nbU; j ++) {
            Vec3 acc(0., 0., 0.);
            // calculer les Bernstein avec le nombre de points de controles en u et v

            float u = (float) j / (nbU-1);
            for(int n = 0; n < nbCPv; n ++) {


                float Bi = getBern(n, degV, u);
                for(int m = 0; m < nbCPu; m ++) {

                    float Bj = getBern(m, degU, v);
                    acc += Bi * Bj * gridCP[n][m];
                }

            }

            surfLine.push_back(acc);
            acc = Vec3(0., 0., 0.);
        }

        bezierSurface.push_back(surfLine);

        surfLine.clear();
    }
    return bezierSurface;
}

//points de controles
Vec3 P6(-0.5, 1., 0.);
Vec3 P5(-1., 1., 0.);
Vec3 P2(-1., -1., 0);
Vec3 P3(1., -1., 0.);
Vec3 P4(1., 1., -0.2);
Vec3 P7(0., 1., 0.);
Vec3 P8(0., 1., -1.);
Vec3 P9(0., 0.5, -1.);



//variables pour l'affichage
bool displayBezierSurf = false;
bool toggleCylindric = true;
bool toggleRuled = false;
bool addCtrlPt = false;
float V = 10;
float U = 30;

std::vector<Vec3> ctrlPts;
std::vector<Vec3> ctrlPts2;
std::vector<Vec3> ctrlPts3;
std::vector<std::vector<Vec3>> gridCP;

void draw () {

    ctrlPts.clear();
    ctrlPts2.clear();
    gridCP.clear();

    ctrlPts.push_back(P6);
    ctrlPts.push_back(P2);
    ctrlPts.push_back(P3);

    ctrlPts2.push_back(P7);
    ctrlPts2.push_back(P8);
    ctrlPts2.push_back(P9);

    ctrlPts3.push_back(Vec3(-0.5, 0.5, -0.8));
    ctrlPts3.push_back(Vec3(-0.5, -0.5, 0));

    if(addCtrlPt)
        ctrlPts.push_back(P4);
        
    gridCP.push_back(ctrlPts);
    gridCP.push_back(ctrlPts3);

    Vec3 V0(1., 1., -2.);
    std::vector<std::vector<Vec3>> curveC = getCylindricalSurface(ctrlPts, V0, U, V);
    std::vector<std::vector<Vec3>> ruledSurf = getRuledSurface(ctrlPts, ctrlPts2, U, V);
    std::vector<std::vector<Vec3>> bezierSurf = bezierSurfaceByBernstein(gridCP, gridCP[0].size(), gridCP.size(), U, V);

    //surface cylindrique
    if(toggleCylindric)
        DrawSurface(curveC, Vec3(0., 0.6, 0.));

    //surface réglée
    if(toggleRuled){
        DrawSurface(ruledSurf, Vec3(0., 0.6, 0.));
        DrawCurve(ctrlPts, ctrlPts.size(), Vec3(1., 1., 1.));
        DrawCurve(ctrlPts2, ctrlPts2.size(), Vec3(1., 1., 1.));
    }else {

        DrawCurve(BezierCurveByBernstein(ctrlPts, ctrlPts.size(), U), U, Vec3(1., 0., 0.));
        DrawCurve(BezierCurveByBernstein(ctrlPts2, ctrlPts2.size(), U), U, Vec3(0., 0., 1.));
        DrawCurve(ctrlPts, ctrlPts.size(), Vec3(1., 1., 1.));
        DrawCurve(ctrlPts2, ctrlPts2.size(), Vec3(1., 1., 1.));
    }

    if(displayBezierSurf){

        DrawSurface(bezierSurf, Vec3(0., 0.5, 0.5));
        DrawCurve(ctrlPts, ctrlPts.size(), Vec3(1., 1., 1.));
        DrawCurve(ctrlPts3, ctrlPts3.size(), Vec3(1., 1., 1.));
    }

    // std::cout<<"calcul fait"<<std::endl;
    //points de controles:
    // DrawCurve(ctrlPts3, ctrlPts3.size(), Vec3(1., 1., 1.));


}


void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {

    case '+': // + et - pour modifier la position d'un des points de contrôle
        P3[1] += 0.05;
        P3[0] -= 0.05;
        break;

    case '-': 
        P3[1] -= 0.05;
        P3[0] += 0.05;
        break;   

    case 'b': //afficher ou non les segments de construction pour casteljau
        displayBezierSurf = !displayBezierSurf;
    break;  
    
    case 'r': //afficher la cubique d'hermite de la question 1
        toggleRuled = !toggleRuled;
    break;

    case 'c': //afficher la cubique d'hermite de la question 1
        toggleCylindric = !toggleCylindric;
    break;

    case 'p': //ajouter / supprimer un point de controle
        addCtrlPt = !addCtrlPt;
        
    break;

    
    case 'v': //ajouter des iso paramétriques
        V ++;
    break;

    case 'u': //ajouter des iso paramétriques
        U ++;
    break;

    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}



int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP3 HAI714I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    glutMainLoop ();
    return EXIT_SUCCESS;
}

