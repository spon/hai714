// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include <math.h>
#include "src/Vec3.h"
#include "src/Camera.h"

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;


// ------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}




// ------------------------------------
// rendering.
// ------------------------------------


void DrawCurve(std::vector<Vec3> TabPointsOfCurve, long nbPoints, Vec3 line_color) {
    glBegin(GL_LINE_STRIP);
    glColor3f(line_color[0], line_color[1], line_color[2]);
    float x, y, z;

    for(int i = 0; i < nbPoints; i ++) {
        x = TabPointsOfCurve[i][0];
        y = TabPointsOfCurve[i][1];
        z = TabPointsOfCurve[i][2];
        glVertex3f(x, y ,z);
    }

    glEnd();
}


void DrawLines(std::vector<std::vector<Vec3>> TabPointsOfCurve, long nbPoints, Vec3 line_color) {
    glBegin(GL_LINES);
    Vec3 color = line_color;
    glColor3f(color[0], color[1], color[2]);

    for(size_t i = 0; i < TabPointsOfCurve.size(); i ++) {
        Vec3 v1 = TabPointsOfCurve[i][0];
        Vec3 v2 = TabPointsOfCurve[i][1];
        glVertex3f(v1[0], v1[1] ,v1[2]);
        glVertex3f(v2[0], v2[1] ,v2[2]);
    }

    glEnd();
}

std::vector<Vec3> HermiteCubicCurve(Vec3 P0, Vec3 P1, Vec3 V0, Vec3 V1, long nbU) {
    std::vector<Vec3> points;
    Vec3 Pu(0., 0., 0.);
    float u = 0.;

    for(int i = 0; i < nbU; i ++) {
        u = (float)i/nbU;

        Pu = (pow(2*u, 3) - (pow(3*u, 2)) + 1) * P0 +
            (-pow(2*u, 3) + (pow(3*u, 2))) * P1 +
            ((pow(u, 3) - 2*pow(u, 2)) + u) * V0 + 
            (pow(u, 3) - pow(u, 2)) * V1;
        points.push_back(Pu);
    }
    return points;
}

int fact(int a){
    if(a == 0)
        return 1;
    return a * fact(a - 1);
}

std::vector<Vec3> BezierCurveByBernstein(std::vector<Vec3> TabControlPoint, long nbControlPoint, long nbU) {
    std::vector<Vec3> points;
    float u = 0.;   
    int n = nbControlPoint - 1;
    float Bern = 0.;

    for(int k = 0; k < nbU; k ++) {
        Vec3 Pu(0., 0., 0.);
        u = (float) k/(nbU - 1);
        for(int i = 0; i < nbControlPoint; i ++) {
            Bern = fact(n)/(fact(i) * fact(n - i)) * pow(u, i) * (pow( 1 - u, n - i));
            Pu += Bern * TabControlPoint[i];   
        }

        points.push_back(Pu);
    }

    return points;       
}

Vec3 getPointCasteljau(float u, int k, int i, std::vector<Vec3> P) {
    if(k==0)
        return P[i];

    return (1-u) * getPointCasteljau(u, k-1, i, P) + u * getPointCasteljau(u, k-1, i+1, P);
    
}

std::vector<Vec3> BezierCurveByCasteljau(std::vector<Vec3> ctrl, std::vector<std::vector<Vec3>> &constrP, long nbCp, long nbU) {
    std::vector<Vec3> points;
    float u;
    int n = nbCp - 1;
    for(int i = 0; i < nbU; i ++) {
        u = (float) i / (nbU-1);

        //ajout du point calculé par récursion
        points.push_back(getPointCasteljau(u, n, 0, ctrl));

        //construction des segments ayant servis pour le calcul du point u 
        std::vector<Vec3> segment;
        int nbVertex = 0;

        for(int i = 1; i < n; i ++) {
            for(int k = 0; k <= n-i; k++) {
                segment.push_back(getPointCasteljau(u, i, k, ctrl));
                nbVertex ++;

                //avoir des segments continus sur une même itération
                if(nbVertex == 2){
                    Vec3 lastPoint = segment[1];
                    constrP.push_back(segment);
                    segment.clear();

                    segment.push_back(lastPoint);

                    nbVertex = 1;
                }

            }
            
            nbVertex = 0;
            segment.clear();
        }
    }

    return points;
}


//points de controles
Vec3 P6(-0.5, 1., 0.);
Vec3 P5(-1., 1., 0.);
Vec3 P2(-1., -1., 0);
Vec3 P3(1., -1., 0.);
Vec3 P4(1., 1., 0.);

//points pour la cubique d'Hermite
Vec3 P0(0., 0., 0.);
Vec3 P1(2., 0., 0.);

//tableau pour stocker les segments de construction
std::vector<std::vector<Vec3>> constrPoints;

//variables pour l'affichage
bool displayCastelpts = false;
bool toggleBernCastel = true;
bool toggleHermite = false;
bool addCtrlPt = false;

void draw () {
    std::vector<Vec3> ctrlPts;
    ctrlPts.push_back(P6);
    ctrlPts.push_back(P5);
    ctrlPts.push_back(P2);
    ctrlPts.push_back(P3);

    if(addCtrlPt)
        ctrlPts.push_back(P4);
        

    Vec3 V0(1., 1., 0);
    Vec3 V1(1., -1., 0.);
    constrPoints.clear();

    //points de controles:
    DrawCurve(ctrlPts, ctrlPts.size(), Vec3(1., 1., 1.));

    if(toggleHermite)
        DrawCurve(HermiteCubicCurve(P0, P1, V0, V1, 50), 50, Vec3(0., 0., 0.8));

    //dessin de la courbe avec Bernstein si true, avec Casteljau sinon
    if(toggleBernCastel){  
        DrawCurve(BezierCurveByBernstein(ctrlPts, ctrlPts.size(), 50), 50, Vec3(0.8, 0.5, 0));
    }else{

        DrawCurve(BezierCurveByCasteljau(ctrlPts,constrPoints, ctrlPts.size(), 50), 50, Vec3(1., 0., 0.));
        if(displayCastelpts)
            DrawLines(constrPoints, constrPoints.size(), Vec3(0., 0.5, 0.));
    }

}


void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {

    case '+': // + et - pour modifier la position d'un des points de contrôle
        P3[1] += 0.05;
        P3[0] -= 0.05;
        break;

    case '-': 
        P3[1] -= 0.05;
        P3[0] += 0.05;
        break;   

    case 'd': //afficher ou non les segments de construction pour casteljau
        displayCastelpts = !displayCastelpts;
    break;  
    
    case 't': //afficher la courbe selon casteljau ou bernstein
        toggleBernCastel = !toggleBernCastel;
    break;
    
    case 'h': //afficher la cubique d'hermite de la question 1
        toggleHermite = !toggleHermite;
    break;

    case 'p': //ajouter / supprimer un point de controle
        addCtrlPt = !addCtrlPt;
        
    break;

    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}



int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("TP3 HAI714I");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    glutMainLoop ();
    return EXIT_SUCCESS;
}

